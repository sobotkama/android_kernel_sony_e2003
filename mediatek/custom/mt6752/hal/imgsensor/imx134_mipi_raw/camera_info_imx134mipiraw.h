#ifndef _CAMERA_INFO_IMX134_MIPI_RAW_H
#define _CAMERA_INFO_IMX134_MIPI_RAW_H

/*******************************************************************************
*   Configuration
********************************************************************************/
#define SENSOR_ID                           IMX134_SENSOR_ID
#define SENSOR_DRVNAME                      SENSOR_DRVNAME_IMX134_MIPI_RAW
#define INCLUDE_FILENAME_ISP_REGS_PARAM     "camera_isp_regs_imx134mipiraw.h"
#define INCLUDE_FILENAME_ISP_PCA_PARAM      "camera_isp_pca_imx134mipiraw.h"
#define INCLUDE_FILENAME_ISP_LSC_PARAM      "camera_isp_lsc_imx134mipiraw.h"
#define INCLUDE_FILENAME_TSF_PARA           "camera_tsf_para_imx134mipiraw.h"
#define INCLUDE_FILENAME_TSF_DATA           "camera_tsf_data_imx134mipiraw.h"
#define INCLUDE_FILENAME_FLASH_AWB_PARA     "camera_flash_awb_para_imx134mipiraw.h"
#define INCLUDE_FILENAME_FEATURE_PARA       "camera_feature_para_imx134mipiraw.h"

/*******************************************************************************
*
********************************************************************************/

#endif
