#ifndef __CUST_RTC_H__
#define __CUST_RTC_H__

/*
 * Default values for RTC initialization
 * Year (YEA)        : 1970 ~ 2037
 * Month (MTH)       : 1 ~ 12
 * Day of Month (DOM): 1 ~ 31
 */
#define RTC_DEFAULT_YEA		2010
#define RTC_DEFAULT_MTH		1
#define RTC_DEFAULT_DOM		1
//[LY28] ==> disable SPAR for power-on failure issue, modified by Jimmy@CCI
//#define RTC_2SEC_REBOOT_ENABLE  1
#define RTC_2SEC_REBOOT_ENABLE  0
//[LY28] <== disable SPAR for power-on failure issue, modified by Jimmy@CCI
#define RTC_2SEC_MODE		0
#endif /* __CUST_RTC_H__ */
