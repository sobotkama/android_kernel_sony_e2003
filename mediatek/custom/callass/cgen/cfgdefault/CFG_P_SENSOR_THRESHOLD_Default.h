/*
 * Copyright (C) 2014 Sony Mobile Communications AB.
 * All rights, including trade secret rights, reserved.
 */

#ifndef _CFG_P_SENSOR_THRESHOLD_D_H
#define _CFG_P_SENSOR_THRESHOLD_D_H

FILE_P_SENSOR_THRESHOLD_STRUCT p_sensor_threshold_default =
{
    {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
    0,
    0,
    0,
    0,
    0,
};

#endif

