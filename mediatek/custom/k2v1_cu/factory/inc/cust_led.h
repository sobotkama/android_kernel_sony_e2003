#ifndef FTM_CUST_LED_H
#define FTM_CUST_LED_H
//B 2014/07/30 Jiahan Li
#define CUST_LED_HAVE_RED
#define CUST_LED_HAVE_GREEN
#define CUST_LED_HAVE_BLUE
//E 2014/07/30 Jiahan Li
#define CUST_HAVE_NLEDS
#endif /* FTM_CUST_LED_H */
